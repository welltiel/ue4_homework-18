﻿#include <iostream>
#include <string>

using namespace std;

template <typename T> class Stack
{
private:
    int index;
    T* arr;
    void switchArrs()
    {
        T* tempArr = new T[index + 2];

        for (int i = 0; i < (index + 1); i++)
            tempArr[i] = arr[i];

        delete[] arr;

        arr = tempArr;
    }
public:
    Stack()
    {
        index = -1;
        arr = new T[1];
    }

    void push(T elem)
    {
        index++;
        arr[index] = elem;
        switchArrs();
    }

    void pop()
    {
        if (index >= 0)
        {
            index--;

            switchArrs();
        }
        else
        {
            cout << "The array is empty" << endl;
        }

    }

    void top()
    {
        if (index < 0)
            cout << "The array is empty" << endl;
        else
            cout << "Top element is: " << arr[index] << endl;
    }
};

int main()
{
    Stack<string> stack;
    string inputLine;

    while (true)
    {
        cout << "To push string item to stack - input 1, to remove top item - input 2, to show top item - input 3:" << endl;
        getline(cin, inputLine);

        switch (stoi(inputLine))
        {
        case 1:
            cout << "Input a line:" << endl;
            getline(cin, inputLine);
            stack.push(inputLine);
            break;
        case 2:
            stack.pop();
            break;
        case 3:
            stack.top();
            break;
        default:
            cout << "Incorrect input." << endl;
            break;
        }
    }

    return 0;
}
